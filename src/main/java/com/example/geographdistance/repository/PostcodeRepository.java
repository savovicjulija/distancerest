package com.example.geographdistance.repository;

import com.example.geographdistance.entity.Postalcode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PostcodeRepository extends JpaRepository<Postalcode, String> {

    Optional<Postalcode> findByPostcode(String postcode);
}
