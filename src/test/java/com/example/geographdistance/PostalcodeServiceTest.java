package com.example.geographdistance;

import com.example.geographdistance.dto.DistanceCalculatorDTO;
import com.example.geographdistance.dto.UpdateCoordinatesDTO;
import com.example.geographdistance.entity.Postalcode;
import com.example.geographdistance.exception.PostalcodeNotFoundException;
import com.example.geographdistance.repository.PostcodeRepository;
import com.example.geographdistance.service.PostcodeServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.Mockito.when;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class PostalcodeServiceTest {

    @Mock
    private PostcodeRepository postcodeRepository;

    @InjectMocks
    private PostcodeServiceImpl postcodeLatLngServiceImpl;

    @Test
    public void testFindByPostcode() throws PostalcodeNotFoundException {
        //prepare
        Postalcode expected = new Postalcode("AB21 9AB", 57.189905, -2.170814);
        expected.setId(69L);
        when(postcodeLatLngServiceImpl.findByPostcode("AB21 9AB")).thenReturn(Optional.ofNullable(expected));

        //excercise
        Optional<Postalcode> actual = postcodeLatLngServiceImpl.findByPostcode("AB21 9AB");

        //verify
        assertEquals(Optional.ofNullable(expected), actual);

    }

    @Test
    public void testGetDistanceBetweenTwoLocations() throws PostalcodeNotFoundException {
        //prepare
        Postalcode location1 = new Postalcode("AB21 9AR", 57.178494050, -2.173159718);//78 ID
        Postalcode location2 = new Postalcode("AB21 9BB", 57.17977019, -2.185339771); //id 84

        when(postcodeRepository.findByPostcode("AB21 9AR")).thenReturn(Optional.ofNullable(location1));
        when(postcodeRepository.findByPostcode("AB21 9BB")).thenReturn(Optional.ofNullable(location2));
        //  when(postcodeLatLngRepository.findByPostcode("AB21 9BB").get()).thenReturn(location2);

        //excercise
        DistanceCalculatorDTO actual = postcodeLatLngServiceImpl.getDistanceBetweenTwoLocations(location1.getPostcode(), location2.getPostcode());
        DistanceCalculatorDTO expected = new DistanceCalculatorDTO(location1.toString(), location2.toString(), 0.7476716584324776);

        //verify
        assertEquals(Optional.ofNullable(expected).get().getDistance(), actual.getDistance(), 0.0);
    }

    @Test
    public void testUpdateCoordinates() {
        //prepare
        Postalcode location1 = new Postalcode("AB21 9BH", 57.1766626, -2.178659);
        UpdateCoordinatesDTO expected = new UpdateCoordinatesDTO(57.1766626, -2.178659);
        String postcode = "AB21 9BH";
        when(postcodeRepository.findByPostcode("AB21 9BH")).thenReturn(Optional.ofNullable(location1));

        //excercise
        UpdateCoordinatesDTO actual = postcodeLatLngServiceImpl.updateCoordinates(postcode, expected);

        //verify
        assertEquals(Optional.ofNullable(expected).get().getLatitude(), actual.getLatitude(), 0.0);
        assertEquals(Optional.ofNullable(expected).get().getLongitude(), actual.getLongitude(), 0.0);

    }
}
