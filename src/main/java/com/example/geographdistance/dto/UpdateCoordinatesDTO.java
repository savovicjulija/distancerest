package com.example.geographdistance.dto;

import com.example.geographdistance.entity.Postalcode;

import javax.validation.constraints.NotNull;

public class UpdateCoordinatesDTO {

    @NotNull
    private Double latitude;
    @NotNull
    private Double longitude;

    public UpdateCoordinatesDTO() {
    }


    public static UpdateCoordinatesDTO createNewUpdate(Postalcode location) {
        return new UpdateCoordinatesDTO(location);
    }

    private UpdateCoordinatesDTO(Postalcode location) {
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();
    }

    public UpdateCoordinatesDTO(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "UpdateCoordinatesDTO{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
