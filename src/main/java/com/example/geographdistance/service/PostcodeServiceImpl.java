package com.example.geographdistance.service;


import com.example.geographdistance.dto.DistanceCalculatorDTO;
import com.example.geographdistance.dto.UpdateCoordinatesDTO;
import com.example.geographdistance.entity.Postalcode;
import com.example.geographdistance.exception.InvalidPostcodeException;
import com.example.geographdistance.exception.PostalcodeNotFoundException;
import com.example.geographdistance.repository.PostcodeRepository;
import com.example.geographdistance.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PostcodeServiceImpl implements PostcodeService {

    @Autowired
    private PostcodeRepository postcodeRepository;

    @Override
    public Optional<Postalcode> findByPostcode(String postcode) throws InvalidPostcodeException {
        if (!Util.isValidPostcode(postcode) == true) {
            throw new InvalidPostcodeException("Postalcode isn`t valid");
        }
        Optional<Postalcode> postcodeLatLng = postcodeRepository.findByPostcode(postcode);
        return postcodeLatLng;
    }

    public DistanceCalculatorDTO getDistanceBetweenTwoLocations(String postcode1, String postcode2) throws InvalidPostcodeException {
        if ((!Util.isValidPostcode(postcode1) == true) || (!Util.isValidPostcode(postcode2) == true)) {
            throw new InvalidPostcodeException("Postalcode isn`t valid");
        }
        Optional<Postalcode> location1 = postcodeRepository.findByPostcode(postcode1);
        Optional<Postalcode> location2 = postcodeRepository.findByPostcode(postcode2);

        String locationOne = location1.get().toString();
        String locationTwo = location2.get().toString();

        Double result = Util.calculateDistance(location1.get().getLatitude(), location1.get().getLongitude(), location2.get().getLatitude(), location2.get().getLongitude());

        DistanceCalculatorDTO distanceCalculatorDTO = new DistanceCalculatorDTO(locationOne, locationTwo, result);

        return distanceCalculatorDTO;
    }

    public UpdateCoordinatesDTO updateCoordinates(String postcode, UpdateCoordinatesDTO location) throws PostalcodeNotFoundException {
        if (postcode == null || postcode.isEmpty()) {
            throw new InvalidPostcodeException("Postalcode can`t be empty");
        }
        Optional<Postalcode> ourLocation = postcodeRepository.findByPostcode(postcode);
        if (!ourLocation.isPresent()) {
            throw new InvalidPostcodeException("Postalcode doesen`t exist!");
        }

        Postalcode postalcodeLatLng = ourLocation.get();
        postalcodeLatLng.setLatitude(location.getLatitude());
        postalcodeLatLng.setLongitude(location.getLongitude());

        postcodeRepository.save(postalcodeLatLng);
        return UpdateCoordinatesDTO.createNewUpdate(postalcodeLatLng);
    }
}

