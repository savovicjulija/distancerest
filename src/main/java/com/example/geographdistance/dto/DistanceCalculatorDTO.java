package com.example.geographdistance.dto;

import com.example.geographdistance.entity.Postalcode;

import javax.validation.constraints.NotNull;

public class DistanceCalculatorDTO {

    @NotNull
    private String postcode1;
    @NotNull
    private String postcode2;

    private double distance;
    private final String unit = "km";


    public DistanceCalculatorDTO() {
    }

    public DistanceCalculatorDTO(String postcode1, String postcode2, double distance) {
        this.postcode1 = postcode1;
        this.postcode2 = postcode2;
        this.distance = distance;
    }

    public String getPostcode1() {
        return postcode1;
    }

    public void setPostcode1(String postcode1) {
        this.postcode1 = postcode1;
    }

    public String getPostcode2() {
        return postcode2;
    }

    public void setPostcode2(String postcode2) {
        this.postcode2 = postcode2;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getUnit() {
        return unit;
    }

    @Override
    public String toString() {
        return
                "postcode1='" + postcode1 + '\'' +
                        ", postcode2='" + postcode2 + '\'' +
                        ", distance= " + String.format("%.4f", distance) +
                        ", unit: " + unit;

    }
}
