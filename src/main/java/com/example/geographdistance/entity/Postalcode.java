package com.example.geographdistance.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Optional;

@Entity
@Table(name = "postcodelatlng")
public class Postalcode {

    @Id
    @GeneratedValue
    private Long id;
    private String postcode;
    private Double latitude;
    private Double longitude;

    public Postalcode() {
    }

    public Postalcode(String postcode, Double latitude, Double longitude) {
        this.postcode = postcode;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getLatitude() {

        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append(postcode + "  ")
                .append("latitude :" + latitude + "  ")
                .append("longitude :" + longitude)
                .toString();
    }
}
