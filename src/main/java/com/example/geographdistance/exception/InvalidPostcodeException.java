package com.example.geographdistance.exception;

public class InvalidPostcodeException extends PostalcodeNotFoundException {

    public InvalidPostcodeException() {
        super();
    }

    public InvalidPostcodeException(String message) {
        super(message);
    }

}
