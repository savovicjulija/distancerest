package com.example.geographdistance.util;

import com.example.geographdistance.util.Util;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

@RunWith(value = Parameterized.class)
public class UtilTestCalculateDistance {

    private Double latitude;
    private Double longitude;
    private Double latitude2;
    private Double longitude2;
    private Double expected;

    // Inject via constructor

    public UtilTestCalculateDistance(Double latitude, Double longitude, Double latitude2, Double longitude2, Double expected) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.latitude2 = latitude2;
        this.longitude2 = longitude2;
        this.expected = expected;
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {57.221883, -2.273318, 57.21026483, -2.180907185, 5.711971142049295},
                {57.12427377, -2.127189644, 57.17632981, -2.1790872, 6.580559205326414},
                {57.14416516, -2.114847768, 57.17372803, -2.183027222, 5.263962337316341},
                {57.176329811, -2.1790871, 57.17977019, -2.185339771, 0.5369972865284951}
        });
    }

    @Test
    public void TestDistanceCalculator() {
        assertThat(Util.calculateDistance(latitude, longitude, latitude2, longitude2), is(expected));
    }
}
