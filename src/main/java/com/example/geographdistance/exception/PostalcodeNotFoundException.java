package com.example.geographdistance.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class PostalcodeNotFoundException extends RuntimeException {

    public PostalcodeNotFoundException() {
    }

    public PostalcodeNotFoundException(String message) {

        super(message);
    }

}
