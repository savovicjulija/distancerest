package com.example.geographdistance.util;

import com.example.geographdistance.util.Util;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

@RunWith(value = Parameterized.class)
public class UtilTestIsValidPostalcode {

    @Parameter
    public String postalcode;

    @Parameters
    public static Object[] data() {
        return new Object[]{
                "AB10 1XG",
                "AB10 6RN",
                "AB10 7JB",
                "AB11 5QN",
                "AB21 9BU"
        };
    }

    @Test
    public void testUtil() {
        assertThat(Util.isValidPostcode(postalcode), is(true));
    }
}
