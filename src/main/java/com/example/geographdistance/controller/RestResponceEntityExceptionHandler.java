package com.example.geographdistance.controller;

import com.example.geographdistance.exception.InvalidPostcodeException;
import com.example.geographdistance.exception.PostalcodeNotFoundException;
import com.sun.javaws.exceptions.ErrorCodeResponseException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class RestResponceEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {PostalcodeNotFoundException.class, InvalidPostcodeException.class})
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, HttpStatus status, WebRequest request) {
        String bodyOfResponse = "ControllerAdvice: Input isn`t valid!";
        ex.getMessage();
        request.getDescription(false);
        return new ResponseEntity<>(bodyOfResponse, status);
    }
}
