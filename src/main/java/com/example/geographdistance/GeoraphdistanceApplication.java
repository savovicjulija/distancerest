package com.example.geographdistance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeoraphdistanceApplication {

    public static void main(String[] args) {
        SpringApplication.run(GeoraphdistanceApplication.class, args);
    }
}
