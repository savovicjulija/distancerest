package com.example.geographdistance.controller;


import com.example.geographdistance.dto.UpdateCoordinatesDTO;
import com.example.geographdistance.exception.PostalcodeNotFoundException;
import com.example.geographdistance.service.PostcodeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/")
public class PostcodeController {

    @Autowired
    private PostcodeServiceImpl postcodeLatLngService;

    @GetMapping("/{postcode}")
    public ResponseEntity<Object> retrieveLocation(@PathVariable String postcode) throws PostalcodeNotFoundException {
        return ResponseEntity.ok(postcodeLatLngService.findByPostcode(postcode).get());
    }

    @GetMapping("/{postcode1}/{postcode2}")
    public ResponseEntity<Object> getDistance(@PathVariable String postcode1, @PathVariable String postcode2) throws PostalcodeNotFoundException {
        return ResponseEntity.ok(postcodeLatLngService.getDistanceBetweenTwoLocations(postcode1, postcode2));
    }

    @PostMapping("/updateCoordinates/{postcode}")
    public ResponseEntity<UpdateCoordinatesDTO> updateCoordinates(@PathVariable String postcode, @RequestBody @Valid UpdateCoordinatesDTO location) throws PostalcodeNotFoundException {
        return ResponseEntity.ok(postcodeLatLngService.updateCoordinates(postcode, location));
    }
}
