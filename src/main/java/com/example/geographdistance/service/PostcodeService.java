package com.example.geographdistance.service;

import com.example.geographdistance.dto.DistanceCalculatorDTO;
import com.example.geographdistance.dto.UpdateCoordinatesDTO;
import com.example.geographdistance.entity.Postalcode;
import com.example.geographdistance.exception.PostalcodeNotFoundException;

import java.util.Optional;


public interface PostcodeService {

    Optional<Postalcode> findByPostcode(String postcode) throws PostalcodeNotFoundException;

    DistanceCalculatorDTO getDistanceBetweenTwoLocations(String postcode1, String postcode2) throws PostalcodeNotFoundException;

    UpdateCoordinatesDTO updateCoordinates(String postcode, UpdateCoordinatesDTO location) throws PostalcodeNotFoundException;

}


